<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\paymentcontroller;
use App\Http\Controllers\frontendcontroller;
use App\Http\Controllers\admin\ticketscontroller;
use App\Http\Controllers\admin\flightscontroller;
use App\Http\Controllers\admin\usercontroller;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::group(['middleware' => ['auth']], function () {
Route::post('/payment', [paymentcontroller::class, 'payment']);
Route::get('/result', [frontendcontroller::class, 'result']);
Route::get('/Booking', [frontendcontroller::class, 'Traveller']);

Route::post('/buy', [paymentcontroller::class, 'buy']);
});
Route::get('/result', [frontendcontroller::class, 'result']);
Route::get('/profile', [frontendcontroller::class, 'profile']);
Auth::routes();
Route::group(['middleware' => ['admin']], function () {
Route::get('admin', function () {
    return view('admin/index');
});
Route::get('/admin/dashboard', [ticketscontroller::class, 'dashboard']);
Route::resource('/admin/tickets', ticketscontroller::class);
Route::resource('/admin/flights', flightscontroller::class);
Route::resource('/admin/users', usercontroller::class);
});

