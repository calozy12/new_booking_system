<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\flightscontroller;
use App\Http\Controllers\api\ticketscontroller;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
use App\Models\User;
Route::get('/user/{user}', function (User $user) {
   return $user;
});
Route::resource('/tickets', ticketscontroller::class);
Route::resource('/flights', flightscontroller::class);
Route::get('cities', function () {
    return view('cities');
});
