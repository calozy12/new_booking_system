<?php
use App\models\flights;
use App\models\countries;
use App\models\cities;
?>
@include ("partials.header")
<style>
    body
    {
        background-color:#e5f0f9
    }
    @media only screen and (max-width: 768px) {
        .cll {
            display: none;
        }
    }

    @media only screen and (max-width: 768px) {
        .cll2 {
            display: inline-block;
        }
    }

    @media only screen and (min-width: 768px) {
        .cll2 {
            display: none;
        }
    }

    @media only screen and (max-width: 700px) {
        .fade {
            margin-bottom: 17px;
        }
    }

    @media only screen and (max-width: 576px) {
        .fade {
            width: 100%;
            display: inline-block;
        }

        .FADE2 {
            width: 25%;
            display: inline-block;
        }

        .FADE3 {
            width: 100%;
            display: block;
        }

        .cll2 {
            display: none;
        }
    }
    *
    {
        justify-content: center;
    }
</style>
<br>
<center>
    <ul style="margin-bottom: 5px" class="nav justify-content-center" id="myTab" role="tablist">
        <li class="nav-item text-center" role="presentation">
            <a style="background-color:white;color: black" class="nav-link active btnnn1" id="home-tab"
                data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">recommended &nbsp;
                |</a>
        </li>
        <li class="nav-item text-center" role="presentation">
            <a style="background-color:white;color: black" class="nav-link btnnn2" id="profile-tab" data-toggle="tab"
                href="#profile" role="tab" aria-controls="profile" aria-selected="false">Quickest &nbsp; |</a>
        </li>
        <li class="nav-item text-center" role="presentation">
            <a style="background-color:white;color: black" class="nav-link btnnn3" id="contact-tab" data-toggle="tab"
                href="#contact" role="tab" aria-controls="contact" aria-selected="false">Earliest &nbsp; |</a>
        </li>
        <li class="nav-item text-center" role="presentation">
            <a style="background-color:white;color: black" class="nav-link btnnn4" id="recommended-tab"
                data-toggle="tab" href="#recommended" role="tab" aria-controls="recommended"
                aria-selected="false">Cheapest</a>
        </li>
    </ul>
</center>
<center>
<div style="margin-bottom: 5px;" class="container cll2">
    <div class="row">
        <div style="background-color:white;border:2px solid #eee" class="col-md-3 col-sm-3 col-xs-1">
            <i class="fas fa-filter"></i>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-2 col-sm-3 col-xs-1">
            <i class="fas fa-plane"></i>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-2 col-sm-3 col-xs-1">
            <i class="far fa-calendar-alt"></i>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-2 col-sm-3 col-xs-1">
            <i class="far fa-clock"></i>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-3 col-sm-12 col-xs-12">
            <i class="fas fa-tags"></i>
        </div>
    </div>
</div>
</center>

<div style="margin-bottom: 5px" class="container cll">
    <div class="row">
        <div style="background-color:white;border:2px solid #eee" class="col-md-3">
            <center><h4 style="color: black;font-weight:lighter;">Airline</h4></center>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-2">
        <center><h4 style="color: black;font-weight:lighter;">Depart</h4></center>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-2">
        <center><h4 style="color: black;font-weight:lighter;">Duration</h4></center>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-2">
        <center><h4 style="color: black;font-weight:lighter;">Arrive</h4></center>
        </div>
        <div style="background-color:white;border:2px solid #eee" class="col-md-3">
        <center><h4 style="color: black;font-weight:lighter;">Price</h4></center>
        </div>
    </div>
</div>

<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="container">
        <?php
$flights=flights::where('departure_city',$_GET['flying_from'])->where('arrival_city',$_GET['flying_to'])->where('date','=',$_GET['flying_date'])->orderby('id','asc')->get();
        ?>
            @foreach($flights as $flight)
            <div class="row mb-1">
            <div class="col-md-3  col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
<?php
if($_GET['class']==1){
    $flightprice=$flight->buisness_price;
}
if($_GET['class']==0){
        $flightprice=$flight->best_fare;
}
$city=cities::find($_GET['flying_from']);
$city1=cities::find($_GET['flying_to']);
$index_image=$flight->airline_logo;?>
                   <center><img style='width:100px;' src="{{ asset ('storage/flights/'.$index_image) }}">
                    <br>
                    <br>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city->name}}</h4><span style="display: inline-block">({{$flight->_from}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->departure_date}}</h6>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h5 style="color: black;margin-bottom:10px;">{{$flight->duration}} hour(s)</h5></center>
                    <center>
                        <div class="container">
                            <hr>
                        </div>
                    </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city1->name}}</h4><span style="display: inline-block">({{$flight->_to}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->arrival_date}}</h6>
                </center>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 FADE3" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;">US {{$flightprice}}$</h4><br></center>
                            <center>
                                <form action="/Booking" method="get">
                                    <input type="text" name="id" value="{{$flight->id}}" hidden>
                                    <input type="text" name="infants" value="{{$_GET['infants']}}" hidden>
                                    <input type="text" name="adults" value="{{$_GET['adults']}}" hidden>
                                    <input type="text" name="children" value="{{$_GET['children']}}" hidden>
                                    <input type="text" name="class" value="{{$_GET['class']}}" hidden>
                                    <button type="submit" class="btn" style="background-color: #eee;margin:5px;">Book Now</button>
                                </form>
                            </center>
                </div>
            </div>
@endforeach
@if(!isset($flights[0]))
<center>
    <h1 class="fontcolor" style="font-weight:lighter">Sorry no flights here!</h1>
    <center>
        @endif
        </div>
        <br>
    </div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
        <div class="container">
<?php
$flights=flights::where('departure_city',$_GET['flying_from'])->where('arrival_city',$_GET['flying_to'])->where('date','=',$_GET['flying_date'])->orderby('duration','asc')->get();
        ?>
            @foreach($flights as $flight)
            <div class="row mb-1">
            <div class="col-md-3  col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
<?php
if($_GET['class']==1){
    $flightprice=$flight->buisness_price;
}
if($_GET['class']==0){
        $flightprice=$flight->best_fare;
}
$city=cities::find($_GET['flying_from']);
$city1=cities::find($_GET['flying_to']);
$index_image=$flight->airline_logo;?>
                   <center><img style='width:100px;' src="{{ asset ('storage/flights/'.$index_image) }}">
                    <br>
                    <br>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city->name}}</h4><span style="display: inline-block">({{$flight->_from}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->departure_date}}</h6>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h5 style="color: black;margin-bottom:10px;">{{$flight->duration}} hour(s)</h5></center>
                    <center>
                        <div class="container">
                            <hr>
                        </div>
                    </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city1->name}}</h4><span style="display: inline-block">({{$flight->_to}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->arrival_date}}</h6>
                </center>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 FADE3" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;">US {{$flightprice}}$</h4><br></center>
                            <center>
                                <form action="/Booking" method="get">
                                    <input type="text" name="id" value="{{$flight->id}}" hidden>
                                    <input type="text" name="infants" value="{{$_GET['infants']}}" hidden>
                                    <input type="text" name="adults" value="{{$_GET['adults']}}" hidden>
                                    <input type="text" name="children" value="{{$_GET['children']}}" hidden>
                                    <input type="text" name="class" value="{{$_GET['class']}}" hidden>
                                    <button type="submit" class="btn" style="background-color: #eee;margin:5px;">Book Now</button>
                                </form>
                            </center>
                </div>
            </div>
@endforeach
@if(!isset($flights[0]))
<center>
    <h1 class="fontcolor" style="font-weight:lighter">Sorry no flights here!</h1>
    <center>
        @endif
        </div>
        <br>
    </div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="content-tab">
        <div class="container">
<?php
$flights=flights::where('departure_city',$_GET['flying_from'])->where('arrival_city',$_GET['flying_to'])->where('date','=',$_GET['flying_date'])->orderby('date','asc')->get();
        ?>
           @foreach($flights as $flight)
            <div class="row mb-1">
            <div class="col-md-3  col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
<?php
if($_GET['class']==1){
    $flightprice=$flight->buisness_price;
}
if($_GET['class']==0){
        $flightprice=$flight->best_fare;
}
$city=cities::find($_GET['flying_from']);
$city1=cities::find($_GET['flying_to']);
$index_image=$flight->airline_logo;?>
                   <center><img style='width:100px;' src="{{ asset ('storage/flights/'.$index_image) }}">
                    <br>
                    <br>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city->name}}</h4><span style="display: inline-block">({{$flight->_from}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->departure_date}}</h6>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h5 style="color: black;margin-bottom:10px;">{{$flight->duration}} hour(s)</h5></center>
                    <center>
                        <div class="container">
                            <hr>
                        </div>
                    </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city1->name}}</h4><span style="display: inline-block">({{$flight->_to}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->arrival_date}}</h6>
                </center>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 FADE3" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;">US {{$flightprice}}$</h4><br></center>
                    <center>
                            <form action="/Booking" method="get">
                                <input type="text" name="id" value="{{$flight->id}}" hidden>
                                <input type="text" name="infants" value="{{$_GET['infants']}}" hidden>
                                <input type="text" name="adults" value="{{$_GET['adults']}}" hidden>
                                <input type="text" name="children" value="{{$_GET['children']}}" hidden>
                                <input type="text" name="class" value="{{$_GET['class']}}" hidden>
                                <button type="submit" class="btn" style="background-color: #eee;margin:5px;">Book Now</button>
                            </form>
                            </center>
                </div>
            </div>
@endforeach
@if(!isset($flights[0]))
<center>
    <h1 class="fontcolor" style="font-weight:lighter">Sorry no flights here!</h1>
    <center>
        @endif

        </div>
        <br>
    </div>
    <div class="tab-pane fade" id="recommended" role="tabpanel" aria-labelledby="recommended-tab">
        <div class="container">
        <?php
        if($_GET['class']==1){
$flights=flights::where('departure_city',$_GET['flying_from'])->where('arrival_city',$_GET['flying_to'])->where('date','=',$_GET['flying_date'])->orderby('buisness_price','asc')->get();
        }
        if($_GET['class']==0){
$flights=flights::where('departure_city',$_GET['flying_from'])->where('arrival_city',$_GET['flying_to'])->where('date','=',$_GET['flying_date'])->orderby('best_fare','asc')->get();
        }
        ?>
            @foreach($flights as $flight)
            <div class="row mb-1">
            <div class="col-md-3  col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
<?php
if($_GET['class']==1){
    $flightprice=$flight->buisness_price;
}
if($_GET['class']==0){
        $flightprice=$flight->best_fare;
}
$city=cities::find($_GET['flying_from']);
$city1=cities::find($_GET['flying_to']);
$index_image=$flight->airline_logo;?>
                   <center><img style='width:100px;' src="{{ asset ('storage/flights/'.$index_image) }}">
                    <br>
                    <br>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city->name}}</h4><span style="display: inline-block">({{$flight->_from}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->departure_date}}</h6>
                </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                <center><h5 style="color: black;margin-bottom:10px;">{{$flight->duration}} hour(s)</h5></center>
                    <center>
                        <div class="container">
                            <hr>
                        </div>
                    </center>
                </div>
                <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city1->name}}</h4><span style="display: inline-block">({{$flight->_to}})</span>
                    <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                    <h6 style="color: black;">{{$flight->arrival_date}}</h6>
                </center>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12 FADE3" style="background-color: white;border:2px solid #eee">
                    <center><h4 style="color: royalblue;">US {{$flightprice}}$</h4><br></center>
                            <center>
                                <form action="/Booking"method="get">
                                    <input type="text"name="id"value="{{$flight->id}}" hidden>
                                <input type="text" name="infants" value="{{$_GET['infants']}}" hidden>
                                    <input type="text" name="adults" value="{{$_GET['adults']}}" hidden>
                                    <input type="text" name="children" value="{{$_GET['children']}}" hidden>
                                    <input type="text" name="class" value="{{$_GET['class']}}" hidden>
                                    <button type="submit" class="btn" style="background-color: #eee;margin:5px;">Book Now</button>
                                </form>
                                </center>
                </div>
            </div>
@endforeach

@if(!isset($flights[0]))
<center><h1 class="fontcolor"style="font-weight:lighter">Sorry no flights here!</h1><center>
@endif
        </div>
        <br>
    </div>
</div>


<script>
        $(".btnnn1").click(function () {
        $(".btnnn1").addClass("active2");
        $(".btnnn2").removeClass("active2");
        $(".btnnn3").removeClass("active2");
        $(".btnnn4").removeClass("active2");
    });
    $(".btnnn2").click(function () {
        $(".btnnn2").addClass("active2");
        $(".btnnn1").removeClass("active2");
        $(".btnnn3").removeClass("active2");
        $(".btnnn4").removeClass("active2");
    });
    $(".btnnn3").click(function () {
        $(".btnnn3").addClass("active2");
        $(".btnnn2").removeClass("active2");
        $(".btnnn1").removeClass("active2");
        $(".btnnn4").removeClass("active2");
    });
    $(".btnnn4").click(function () {
        $(".btnnn4").addClass("active2");
        $(".btnnn2").removeClass("active2");
        $(".btnnn3").removeClass("active2");
        $(".btnnn1").removeClass("active2");
    });
</script>

@include ("partials.jslinks")
