<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" />
        <link rel="shortcut icon" href="https://w7.pngwing.com/pngs/773/201/png-transparent-airplane-aircraft-flight-logo-airplane-blue-logo-flight.png">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <script src="https://unpkg.com/vue"></script>
        <script src="https://unpkg.com/marked@0.3.6"></script>
        <script src="https://unpkg.com/lodash@4.16.0"></script>
        @yield('extra_links1')
        <?php
        session_start();
            if(isset($_REQUEST['color'])){
            $color=$_REQUEST['color'];
            $_SESSION['color']=$_REQUEST['color'];
            }elseif(isset($_SESSION['color'])){
            $color=$_SESSION["color"];
            }
            else{$color='white';}

            if($color=='Blue'){
         $footercolor='#206e6e';
         $link='https://pbs.twimg.com/media/DpMKIyAVAAIbsKv.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Yellow'){
         $footercolor='#f89d00';
         $link='https://i.pinimg.com/originals/02/5f/29/025f29a640db04a067d7a540a7b4d004.gif';
         $class='navbar-light';
         $textcolor='#ffffff';
     }

     if($color=='Pink'){
         $footercolor='#F6A2C6';
         $link='https://image.freepik.com/free-photo/travel-concept-airplane-pink-background-place-text_134398-5735.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Green'){
         $footercolor='#dff05f';
         $link='https://images.unsplash.com/photo-1530469641172-8ac15d0a7d6a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
    if($color=='white'){
         $footercolor='#520622';
         $link='https://cutewallpaper.org/21/plane-background-wallpaper/Best-64-Plane-Backgrounds-on-HipWallpaper-Art-Deco-.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Dark'){
        $footercolor='#343A40';
         $link='https://www.chromethemer.com/download/hd-wallpapers/dark-paladin-1920x1080.jpg';
         $class='navbar-dark bg-dark';
         $textcolor='#17A2B8';
     }
    ?>
    <style>
        .my_form{
            border:2px solid {{$textcolor}};
        }
        .list-group-item {
            border:none
        }
        #wrapper {
            overflow-x: hidden;
        }

        #sidebar-wrapper {
            min-height: 100vh;
            margin-left: -15rem;
            -webkit-transition: margin .25s ease-out;
            -moz-transition: margin .25s ease-out;
            -o-transition: margin .25s ease-out;
            transition: margin .25s ease-out;
        }

        #sidebar-wrapper .sidebar-heading {
            padding: 0.875rem 1.25rem;
            font-size: 1.2rem;
        }

        #sidebar-wrapper .list-group {
            width: 15rem;
        }

        #page-content-wrapper {
            min-width: 100vw;
        }

        #wrapper.toggled #sidebar-wrapper {
            margin-left: 0;
        }

        @media (min-width: 768px) {
            #sidebar-wrapper {
                margin-left: 0;
            }

            #page-content-wrapper {
                min-width: 0;
                width: 100%;
            }

            #wrapper.toggled #sidebar-wrapper {
                margin-left: -15rem;
            }
        }
    </style>
<style>
    nav
    {
        background-color: <?php echo $footercolor;?>;
    }
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url("<?php echo $link; ?>");
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
    }
</style>
</head>
<body>



<div class="d-flex" id="wrapper"style="background-color:#dfe6e9">

    <!-- Sidebar -->
    <div class="border-right" id="sidebar-wrapper"style="background-color:{{$footercolor}}">
        <div class="sidebar-heading" style="color:{{$textcolor}};background-color:{{$footercolor}}">Flight Booking </div>
        <div class="list-group list-group-flush"style="background-color:{{$footercolor}}">
        <a href="/admin/dashboard" class="list-group-item list-group-item-action "style="color:{{$textcolor}};background-color:{{$footercolor}}"><i  class="fas fa-home"></i></a>
            <a href="/admin/tickets"class="list-group-item list-group-item-action "style="color:{{$textcolor}};background-color:{{$footercolor}}">Tickets</a>
            <a href="/admin/users"class="list-group-item list-group-item-action "style="color:{{$textcolor}};background-color:{{$footercolor}}">Users</a>
            <a href="/admin/flights" class="list-group-item list-group-item-action "
                style="color:{{$textcolor}};background-color:{{$footercolor}}">Flights</a>
            <a href="/"class="list-group-item list-group-item-action "style="color:{{$textcolor}};background-color:{{$footercolor}}">Website</a>
        </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
