<?php
use App\models\cities;
use App\models\countries;
$citie = cities::orderby('id', 'asc')->get();
        $cities=$citie->groupby('name');
$countries=countries::all();
?>
@include ("admin.partials.header")

<div id="editor">
    <div class="container">
<div class="my_form"style="padding:10px;margin-top:50px">
    <form action="/api/tickets/{{$id}}" method="post" enctype='multipart/form-data'>
<div class="form-group">
        @csrf
        @method('PUT')
    <label>From</label>
    <input :value="tickets._from"name="_from" onchange="country1()" list="from_c"placeholder="choose city" class="form-control country1 _from"name="from_c"required autocomplete="off">
    <datalist id="from_c">
        <?php foreach($countries as $country){
           echo'<option value="'.$country->country_code.'">'.$country->country_enName.'</option>';}?>
    </datalist>
</div>
<div class="form-group">
<div class="cities1"></div>
</div>
<div class="form-group">
    <label>to</label>
    <input :value="tickets._to"name="_to" onchange="country2()" list="from_c"placeholder="choose city" class="form-control _to" name="from_c" required autocomplete="off">
</div>
<div class="form-group">
    <div class="cities2"></div>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">date</label>
    <input name="date" :value="tickets.date" type="date"class="date form-control"required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">time</label>
    <input name="time" :value="tickets.time" type="time" class="time form-control" required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">duration</label>
    <input name="duration" :value="tickets.duration" type="text" class="duration form-control" required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">passport name</label>
    <input name="passport_name" :value="tickets.passport_name" type="text" class="passport_name form-control" required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">passport expiration date</label>
    <input name="passport_expiration_date" :value="tickets.passport_expiration_date" type="date" class="passport_expiration_date form-control" required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">passport number</label>
    <input name="passport_number" :value="tickets.passport_number" type="text" class="passport_number form-control"
        required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">price</label>
    <input name="price" :value="tickets.price" type="text" class="passport_number form-control"
        required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">no_of_tickets</label>
    <input name="no_of_tickets" :value="tickets.no_of_tickets" type="text" class="passport_number form-control"
        required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">phone</label>
    <input name="phone" :value="tickets.phone" type="text" class="passport_number form-control"
        required>
</div>
<div class="form-group">
    <label for="exampleInputPassword1">status</label>
    <select  class="form-control status"name="status">
        <option value="0" v-if="tickets.status==0"selected>Pending</option>
        <option value="0" v-else>Pending</option>
        <option value="1" v-if="tickets.status==1" selected>Approved</option>
        <option value="1" v-else>Approved</option>
        <option value="2" v-if="tickets.status==2" selected>Canceled</option>
        <option value="2" v-else >Canceled</option>
    </select>

</div>
<div class="form-group">
    <label for="exampleInputPassword1">passport image</label>
    <input type="file" class="passport_image"name="passport_image">
    <input type="text":value="tickets.passport_image" name="passport_image1" class="form-control passport_image1"hidden>
</div>
<button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
</div>
@extends ("partials.jslinks")
@section('extra_links')
<script>
function winner() {
var cba=JSON.parse(abc)
editor =new Vue({
el: "#editor",
data: {
tickets:cba
}
});
init1(cba)
init2(cba)
}
var data = "";

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

abc=this.responseText
winner()
}
});

xhr.open("GET", "http://localhost:8000/api/tickets/{{$id}}/edit");

xhr.send(data);
function country1(){
var xhr2 = new XMLHttpRequest();
xhr2.withCredentials = true;
co=$(".country1").val()
xhr2.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities1').html(this.responseText)
}
});

xhr2.open("GET", "http://localhost:8000/api/cities?co="+co+"&class=departure_city&class1=form-control");

xhr2.send();
}
function init1(cba){
var xhr4 = new XMLHttpRequest();
xhr4.withCredentials = true;
co=$(".country1").val()
xhr4.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities1').html(this.responseText)
}

});

xhr4.open("GET", "http://localhost:8000/api/cities?co="+co+"&value="+cba.departure_city+"&class=departure_city&class1=form-control");

xhr4.send();
}
function country2(){
var xhr3 = new XMLHttpRequest();
xhr3.withCredentials = true;
co1=$(".country2").val()
xhr3.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities2').html(this.responseText)
}
});

xhr3.open("GET", "http://localhost:8000/api/cities?co="+co1+"&class=arrival_city&class1=form-control");

xhr3.send();
}
function init2(cba){
var xhr3 = new XMLHttpRequest();
xhr3.withCredentials = true;
co1=$(".country2").val()
xhr3.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities2').html(this.responseText)
}
});

xhr3.open("GET", "http://localhost:8000/api/cities?co="+co1+"&value="+cba.arrival_city+"&class=arrival_city&class1=form-control");

xhr3.send();
}
</script>
@endsection

