@include ("admin.partials.header")

<div id="editor">

<table class="table table-striped table-dark mt-2">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">from</th>
            <th scope="col">to</th>
            <th scope="col">duration</th>
            <th scope="col">date</th>
            <th scope="col">status</th>
            <th scope="col">control</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="ticket in tickets">
            <th scope="row">@{{ ticket.id }}</th>
            <td>@{{ ticket._from }}</td>
            <td>@{{ ticket._to }}</td>
            <td>@{{ ticket.duration }}</td>
            <td>@{{ ticket.date }}</td>
            <td>@{{ ticket.status }}</td>
            <td>
                <a v-bind:href="'tickets/'+ticket.id+'/edit'"class="btn btn-info mr-2">edit</a>
<div class="fefs"style="display:inline">
                <button v-bind:value="ticket.id"v-if="ticket.status=='Pending'" class="btn btn-xs btn-success" onclick="accept(this)">accept</button>
                <button v-bind:value="ticket.id"v-if="ticket.status=='Pending'" class="btn btn-xs btn-danger" onclick="cancel(this)">decline</button>
</div>
            </td>
        </tr>
    </tbody>
</table>

</div>
@extends ("partials.jslinks")
@section('extra_links')
<script>
    var abc;
var data = "";
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;
xhr.addEventListener("readystatechange", function() {
    if(this.readyState === 4) {
abc=this.responseText
winner()
    }
});
function winner() {
  var cba=JSON.parse(abc)
  for(var index = 0; index < cba.length; ++index){
  if(cba[index]['status']==1){
      cba[index]['status']='Accepted'
  }
  if(cba[index]['status']==2){
     cba[index]['status']='Declined'
  }
  if(cba[index].status==0){
cba[index]['status']='Pending'
}
  }
new Vue({
el: "#editor",
data: {
tickets:cba
}
})
}
xhr.open("GET", "http://localhost:8000/api/tickets/");

xhr.send(data);
function accept(thisa) {
var data =
JSON.stringify({"status":"1"});
var id=thisa.value
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
if(this.readyState === 4) {
console.log(this.responseText);
}
});

xhr.open("PUT", "http://localhost:8000/api/tickets/"+id);
xhr.setRequestHeader("Content-Type", "application/json");

xhr.send(data);
location.reload();
}
function cancel(thisa) {
var data =
JSON.stringify({"status":"2"});
var id=thisa.value
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
if(this.readyState === 4) {
console.log(this.responseText);
}
});

xhr.open("PUT", "http://localhost:8000/api/tickets/"+id);
xhr.setRequestHeader("Content-Type", "application/json");

xhr.send(data);
location.reload();
}

</script>
@endsection

