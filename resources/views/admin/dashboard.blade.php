@include ("admin.partials.header")
<?php
use App\Models\User;
use App\Models\tickets;
use App\models\flights;
$flightcount=flights::count();
$usercount=User::count();
$ticketscount=tickets::count();
$ticketscount0=tickets::where('status',0)->count();
$ticketscount1=tickets::where('status',1)->count();
$ticketscount2=tickets::where('status',2)->count();
?>
<div style="z-index: 1;" class="row">
<div class="col-2 mt-5 ml-5">
    <div class="upp p-4" style="background-color:#012e30;color:white">
        <p class='text-right' style="font-size:30px;font-weight:bold">{{$usercount}}</p>
        <p class='text-right' style="font-size:16px;font-weight:bold">Users</p>
        <p class='text-left'><i class="fas fa-3x fa-user"></i></p>

    </div>
    <div class="downn px-4 p-1" style="background-color:#0b3133;color:white">
        <a href="/admin/users"><p class='text-left mt-3' style="font-size:14px;font-weight:300;display:inline;color:white">View
            details</p></a>
        <p class='text-right' style="font-size:15px;display:inline;font-weight:bold"><i class="fas fa-arrow-right"></i>
        </p>
    </div>
</div>
<div class="col-2 mt-5">
    <div class="upp p-4" style="background-color:#01484b;color:white">
        <p class='text-right' style="font-size:30px;font-weight:bold">{{$ticketscount}}</p>
        <p class='text-right' style="font-size:16px;font-weight:bold">All Tickets</p>
        <p class='text-left'><i class="fas fa-3x fa-ticket-alt"></i></p>

    </div>
    <div class="downn px-4 p-1" style="background-color:#012e30;color:white">
        <a href="/admin/tickets"><p class='text-left mt-3' style="font-size:14px;font-weight:300;display:inline;color:white">View
            details</p></a>
        <p class='text-right' style="font-size:15px;display:inline;font-weight:bold"><i class="fas fa-arrow-right"></i>
        </p>
    </div>
</div>
<div class="col-2 mt-5 ">
    <div class="upp p-4" style="background-color:rgb(0, 103, 109);color:white">
        <p class='text-right' style="font-size:30px;font-weight:bold">{{$ticketscount1}}</p>
        <p class='text-right' style="font-size:16px;font-weight:bold">Approved Tickets</p>
        <p class='text-left'><i class="fas fa-3x fa-ticket-alt"></i></p>

    </div>
    <div class="downn px-4 p-1" style="background-color:#01484b;color:white">
        <a href="/admin/tickets"><p class='text-left mt-3' style="font-size:14px;font-weight:300;display:inline;color:white">View
            details</p></a>
        <p class='text-right' style="font-size:15px;display:inline;font-weight:bold"><i class="fas fa-arrow-right"></i>
        </p>
    </div>
</div>
<div class="col-2 mt-5">
    <div class="upp p-4" style="background-color:rgb(0, 153, 161);color:white">
        <p class='text-right' style="font-size:30px;font-weight:bold">{{$ticketscount2}}</p>
        <p class='text-right' style="font-size:16px;font-weight:bold">Canceled Tickets</p>
        <p class='text-left'><i class="fas fa-3x fa-ticket-alt"></i></i></p>

    </div>
    <div class="downn px-4 p-1" style="background-color:rgb(0, 103, 109);color:white">
        <a href="/admin/tickets"><p class='text-left mt-3' style="font-size:14px;font-weight:300;display:inline;color:white">View
            details</p></a>
        <p class='text-right' style="font-size:15px;display:inline;font-weight:bold"><i class="fas fa-arrow-right"></i>
        </p>
    </div>
    </div>
    <div class="col-2 mt-5">
        <div class="upp p-4" style="background-color:rgb(0, 203, 214);color:white">
            <p class='text-right' style="font-size:30px;font-weight:bold">{{$ticketscount0}}</p>
            <p class='text-right' style="font-size:16px;font-weight:bold">Pending Tickets</p>
            <p class='text-left'><i class="fas fa-3x fa-ticket-alt"></i></i></p>

        </div>
        <a href="/admin/tickets"><div class="downn px-4 p-1" style="background-color:rgb(0, 153, 161);color:white">
            <p class='text-left mt-3' style="font-size:14px;font-weight:300;display:inline;color:white">View
                details</p></a>
            <p class='text-right' style="font-size:15px;display:inline;font-weight:bold"><i class="fas fa-arrow-right"></i>
            </p>

</div>
</div>
<div class="col-2 mt-5 ml-5">
    <div class="upp p-4" style="background-color:rgb(0, 192, 202);color:white">
        <p class='text-right' style="font-size:30px;font-weight:bold">{{$flightcount}}</p>
        <p class='text-right' style="font-size:16px;font-weight:bold">flights</p>
        <p class='text-left'><i class="fas fa-3x fa-plane"></i></i></p>

    </div>
    <div class="downn px-4 p-1" style="background-color:rgb(0, 153, 161);color:white">
        <a href="/admin/flights">
            <p class='text-left mt-3' style="font-size:14px;font-weight:300;display:inline;color:white">View
                details</p>
        </a>
        <p class='text-right' style="font-size:15px;display:inline;font-weight:bold"><i class="fas fa-arrow-right"></i>
        </p>
    </div>
</div>
@include ("partials.jslinks")
