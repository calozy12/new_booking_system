@include ("admin.partials.header")

<div id="editor">

<table class="table table-striped table-dark mt-2">
    <thead>
        <tr>
            <th scope="col">id</th>
            <th scope="col">from</th>
            <th scope="col">to</th>
            <th scope="col">duration</th>
            <th scope="col">date</th>
            <th scope="col">available seats</th>
            <th scope="col">best fare price</th>
            <th scope="col">buisness price</th>
            <th scope="col">control</th>
        </tr>
    </thead>
    <tbody>
        <tr v-for="flight in flights">
            <th scope="row">@{{ flight.id }}</th>
            <td>@{{ flight._from }}</td>
            <td>@{{ flight._to }}</td>
            <td>@{{ flight.duration }}</td>
            <td>@{{ flight.date }}</td>
            <td>@{{ flight.available_seats }}</td>
            <td>@{{ flight.best_fare }}</td>
            <td>@{{ flight.buisness_price }}</td>
            <td>
                <a v-bind:href="'flights/'+flight.id+'/edit'"class="btn btn-info mr-2">edit</a>
<div class="fefs"style="display:inline">
                <button v-bind:value="flight.id" class="btn btn-xs btn-danger"onclick="deletea(this)">Delete</button>
</div>
            </td>
        </tr>
    </tbody>
</table>
<a href="flights/create" class="btn btn-info mr-2">create</a>

</div>
@extends ("partials.jslinks")
@section('extra_links')
<script>
    var abc;
var data = "";
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;
xhr.addEventListener("readystatechange", function() {
    if(this.readyState === 4) {
abc=this.responseText

winner()
    }
});
function winner() {
  var cba=JSON.parse(abc)
new Vue({
el: "#editor",
data: {
flights:cba
}
})
}
xhr.open("GET", "http://localhost:8000/api/flights/");

xhr.send(data);
function deletea(thisa) {
var xhr1 = new XMLHttpRequest();
var data = "";
var id=thisa.value


var xhr1 = new XMLHttpRequest();
xhr1.withCredentials = true;

xhr1.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

var abc=this.responseText

location.reload();
}
});
xhr1.open("DELETE", "http://localhost:8000/api/flights/"+id);

xhr1.send(data);
}

</script>
@endsection

