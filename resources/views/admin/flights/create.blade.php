<?php
use App\models\cities;
use App\models\countries;
$citie = cities::orderby('id', 'asc')->get();
        $cities=$citie->groupby('name');
$countries=countries::all();
?>
@include ("admin.partials.header")
<div id="editor">
    <div class="container">
        <div class="my_form" style="padding:10px;margin-top:50px">
            <form action="/api/flights" method="POST" enctype='multipart/form-data'>
                <div class="form-group">
                    <label>From</label>
                    <input name="_from" onchange="country1()" list="from_c" placeholder="choose city"
                        class="form-control country1 _from" required autocomplete="off">
                    <datalist id="from_c">
                        <?php foreach($countries as $country){
           echo'<option value="'.$country->country_code.'">'.$country->country_enName.'</option>';}?>
                    </datalist>
                </div>
                <div class="form-group">
                    <div class="cities1"></div>
                </div>
                <div class="form-group">
                    <label>to</label>
                    <input name="_to" onchange="country2()" list="from_c" placeholder="choose city"
                        class="form-control _to" required autocomplete="off">
                </div>
                <div class="form-group">
                    <div class="cities2"></div>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">date</label>
                    <input name="date" type="date" class="date form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">departure time</label>
                    <input name="departure_date" type="time" class="duration form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">arrival time</label>
                    <input name="arrival_date" type="time" class="duration form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">buisness price</label>
                    <input name="buisness_price" type="number" class="duration form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">best fare price</label>
                    <input name="best_fare" type="number" class="duration form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">timezone</label>
                    <input name="timezone" type="text" class="duration form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">available seats</label>
                    <input name="available_seats" type="number" class="duration form-control" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">airline</label>
                    <input type="text" class="airline_logo form-control" list="airline" name="airline_id">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">airport</label>
                    <input type="text" list="airport" class="airline_logo form-control" name="airport_id">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">airline logo</label>
                    <input type="file" class="airline_logo" name="airline_logo">
                    <input type="text" name="airline_logo1" class="form-control passport_image1" hidden>
                </div>
                <datalist id="airport">
                    <?php foreach($airports as $airport){
        $name=str_replace('Intl','International',$airport['name']);
           $namee=str_replace('Arpt','Airport',$name);
       echo'<option value="'.$airport['id'].'">'.$namee.'</option>';}?>
                </datalist>
                <datalist id="airline">
                    <?php foreach($airlines as $airline){
       echo'<option value="'.$airline['ID'].'">'.$airline['name'].'</option>';}?>
                </datalist>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@extends ("partials.jslinks")
@section('extra_links')
<script>
    function country1(){
    var xhr2 = new XMLHttpRequest();
    xhr2.withCredentials = true;
    co=$(".country1").val()
    xhr2.addEventListener("readystatechange", function() {
    if(this.readyState === 4) {

    $('.cities1').html(this.responseText)
    }
    });

    xhr2.open("GET", "http://localhost:8000/api/cities?co="+co+"&class=departure_city&class1=form-control");

    xhr2.send();
    }
function country2(){
var xhr3 = new XMLHttpRequest();
xhr3.withCredentials = true;
co1=$(".country2").val()
xhr3.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities2').html(this.responseText)
}
});

xhr3.open("GET", "http://localhost:8000/api/cities?co="+co1+"&class=arrival_city&class1=form-control");

xhr3.send();
}
</script>
@endsection
