@include('admin.partials.header')

<br>


<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">name</th>
      <th scope="col">email</th>
      <th scope="col">role</th>

    </tr>
  </thead>
  <tbody>
  <?php foreach($users as $user){ ?>
    <tr>
      <th scope="row">{{$user->id}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->email}}</td>
      <td>{{$user->role}}</td>
      <td><a href='<?php echo "users/".$user->id."/edit" ?>' class="btn btn-info mr-2">edit</a>
        <form action=<?php echo '"users/'.$user['id'].'"';?> method="POST" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="delete">
                                    </form>
                                    </td>

    </tr>
  <?php } ?>
  </tbody>
</table>
<a class="btn btn-primary float-left ml-5"href="users/create">create</a>
  </div>


  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  </script>



        @include('partials.jslinks')
