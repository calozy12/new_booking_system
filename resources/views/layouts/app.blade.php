<!doctype html>
@include('partials.header')
<main class="py-4">
    @yield('content')
</main>
</html>
