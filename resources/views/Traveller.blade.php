<?php
use App\models\flights;
use App\models\cities;
$flight=flights::find($_GET['id']);
if($_GET['class']==1){
    $flightprice=$flight->buisness_price;
}
if($_GET['class']==0){
        $flightprice=$flight->best_fare;
}
$city=cities::find($flight->departure_city);
$city1=cities::find($flight->arrival_city);
$index_image=$flight->airline_logo;?>
@include('partials.header')
<style>
    body
    {
        background-color: #e5f0f9;
    }
    @media only screen and (min-width: 767px) {
        .rightsidebar {
            margin-top: -790px;
        }
    }

    @media only screen and (max-width: 768px) {
        .cll {
            display: none;
        }
    }

    @media only screen and (max-width: 768px) {
        .cll2 {
            display: inline-block;
        }
    }

    @media only screen and (min-width: 768px) {
        .cll2 {
            display: none;
        }
    }

    @media only screen and (max-width: 700px) {
        .fade {
            margin-bottom: 17px;
        }
    }

    @media only screen and (max-width: 576px) {
        .fade {
            width: 100%;
            display: inline-block;
        }

        .FADE2 {
            width: 25%;
            display: inline-block;
        }

        .FADE3 {
            width: 100%;
            display: block;
        }

        .cll2 {
            display: none;
        }
    }
</style>
<br>
    <div class="row">
        <div class="col-md-8">
            <div style="margin-bottom: 5px" class="container cll">
                <div class="row">
                    <div style="background-color:white;border:2px solid #eee" class="col-md-3">
                        <center><h4 style="color: black;font-weight:lighter;">Airline</h4></center>
                    </div>
                    <div style="background-color:white;border:2px solid #eee" class="col-md-2">
                    <center><h4 style="color: black;font-weight:lighter;">Depart</h4></center>
                    </div>
                    <div style="background-color:white;border:2px solid #eee" class="col-md-2">
                    <center><h4 style="color: black;font-weight:lighter;">Duration</h4></center>
                    </div>
                    <div style="background-color:white;border:2px solid #eee" class="col-md-2">
                    <center><h4 style="color: black;font-weight:lighter;">Arrive</h4></center>
                    </div>
                    <div style="background-color:white;border:2px solid #eee" class="col-md-3">
                    <center><h4 style="color: black;font-weight:lighter;">Price</h4></center>
                    </div>
                </div>
            </div>

                    <div class="container">
                        <div class="row mb-1">
                        <div class="col-md-3  col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                               <center><img style='width:100px;' src="{{ asset ('storage/flights/'.$index_image) }}">
                                <br>
                                <br>
                            </center>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                            <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city->name}}</h4><span style="display: inline-block">({{$flight->_from}})</span>
                                <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                                <h6 style="color: black;">{{$flight->departure_date}}</h6>
                            </center>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                            <center><h5 style="color: black;margin-bottom:10px;">{{$flight->duration}} hour(s)</h5></center>
                                <center>
                                    <div class="container">
                                        <hr>
                                    </div>
                                </center>
                            </div>
                            <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                                <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city1->name}}</h4><span style="display: inline-block">({{$flight->_to}})</span>
                                <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                                <h6 style="color: black;">{{$flight->arrival_date}}</h6>
                            </center>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12 FADE3" style="background-color: white;border:2px solid #eee">
                                <center><h4 style="color: royalblue;">{{$flightprice}}$</h4><br></center>
                            </div>
                        </div>

                    </div>
                    </div>
</div>
<br>
<br>
<form action="/payment" method="POST"enctype='multipart/form-data'>
@csrf
<div class="row">
    <div class="col-md-8">
        <div style="background-color:white;width:100%;" class="container">
            <div class="row">
                <label style="background-color: blue;width:100%;color:white;">Please Enter Your Name as in The
                    Passport</label>
                <div class="col-md-1">
                    <center>
                        <h6 style="display:inline-block;margin-top:5px;">Adult<span style="color: red">*</span></h6>
                    </center>
                </div>
                <div class="col-md-4">
                    <input class="form-control" name="name" style="margin-top:5px;" type="text" placeholder="Type Your Name"required>
                </div>

            </div>
            <div class="row">
                @if($errors->any())
                <div class="alert alert-danger" role="alert">
                {{$errors->first()}}
                </div>
                @endif
                <div class="col-md-4">
                    <label style="color:#000;">Passport expiration date</label>
                    <input type="date" name="Passport_expiration_date" class="form-control" style="margin-bottom: 5px"required>
                </div>
                <div class="col-md-4">
                    <label style="color:#000;">Passport Number</label>
                    <input type="text" name="passport_number" class="form-control" style="margin-bottom: 5px"
                        required>
                </div>
                <div class="col-md-4">
                    <label style="font-size:16px">Passport image</label>
                    <input type="file" name="passport_image"required>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-8">
        <div style="background-color:white;width:100%;" class="container">
            <div class="row">
                <label style="background-color: blue;width:100%;color:white;">CONTACT DETAILS</label>
                <div class="col-md-1">
                    <center>
                        <h6 style="display:inline-block;margin-top:5px;">Number</h6>
                    </center>
                </div>
                <div class="col-md-3">
                    <input type="tel" id="phone" class="form-control" value="{{ old('phone') }}" pattern="[0-9]{10}"
                                                        name="first"required>
                                                    <input type="tel" value="{{ old('phone') }}" name="phone" id="realphone" class="form-control"hidden>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-1">
                    <center>
                        <h6 style="display:inline-block;margin-top:5px;">Email</h6>
                    </center>
                </div>

                <div class="col-md-4">
                    <input name="email" type="email" class="form-control" placeholder="example@example.com"
                        style="margin-bottom: 5px">
                </div>
                <div class="col-md-3">
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-8">
        <div class="container">
            <input type="submit" class="btn btn-success" style="margin-top:10px;float:right;" value="Next">

        </div>
    </div>
</div>
<?php
            $no=$_GET['adults']+$_GET['children']+$_GET['infants'];
    $finaleprice=$no*$flightprice
            ?>
<input type="text" name="id" value="{{$flight->id}}" hidden>
                                    <input type="text" name="no_of_tickets" value="{{$no}}" hidden>
                                    <input type="text" name="price" value="{{$finaleprice}}" hidden>
</form>
<br><br><br><br><br><br>
<div class="row">
    <div class="col-md-8"></div>
    <div style="background-color:white;border:2px solid #e4e3e3;width:100%;height:400px" class="col-md-3 rightsidebar">
        <center>
            <h4>Fare Summary</h4>
        </center>
        <br>
        <h5 style="float:left">Base Fare</h5><br><br>
        <h6 style="float: left">{{$_GET['adults']}} Adult(s),{{$_GET['children']}} children,{{$_GET['infants']}} infant(s)</h6>

        <h6 style="float: right;margin:5px;">{{$flightprice}}$</h6>
        <br>
        <hr>
        <br>
        <h5 style="float: left">GRAND TOTAL</h5>
        <h5 style="float: right;margin:5px;">{{$finaleprice}}$</h5>
        <br>
    </div>
</div>
@extends ("partials.jslinks")
@section('extra_links')
<script
            src="https://www.jqueryscript.net/demo/jQuery-International-Telephone-Input-With-Flags-Dial-Codes/build/js/intlTelInput.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/intlTelInput.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/js/utils.js"></script>
        <script>
            $("#phone").intlTelInput({

          });

          $("#phone").keyup(function() {
            var number = $("#phone").intlTelInput("getNumber");
            $("#realphone").val(''+number+'')
            console.log(number)
          });
        jQuery("#phone").on('countrychange', function(){
            var number = $("#phone").intlTelInput("getNumber");
            $("#realphone").val(''+number+'')
            console.log(number)
        })
         function phonenumber(inputtxt)
        {
          var phoneno = /^\d{10}$/;
          if(inputtxt.value.match(phoneno))
          {
              return true;
          }
          else
          {
             alert("Not a valid Phone Number");
             return false;
          }
          }

        </script>

@endsection
