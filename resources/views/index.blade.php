@include ("partials.header")
<?php
use App\models\cities;
use App\models\countries;
$citie = cities::orderby('id', 'asc')->get();
        $cities=$citie->groupby('name');
$countries=countries::all();
setcookie('our-cookies','cookies', time() + (86400 * 30),"/","localhost",TRUE,TRUE);

?>
<style>
    body {
        overflow-x: hidden;
    }

    .contain {
        text-align: center;
    }

    .input-style {
        border: 0;
        border-bottom: 3px solid #fff;
        padding-bottom: .5em;
        font-size: 1em;
        background-color: transparent;
        transition: 0.10s ease-in-out;
        color: white;
        width: 45%;
        display: inline-block;
        margin: 10px;
    }

    .input-style::placeholder {
        color: rgb(206, 206, 206);
    }

    .input-style:hover {
        border-bottom: 3px solid #a6a6a6;
        color: white;
    }

    .input-style:focus {
        outline: 0;
        border: 0;
        border-bottom: 3px solid #0074D9;
        color: white;
    }

    .input-style2 {
        border: 0;
        border-bottom: 3px solid #fff;
        padding-bottom: .5em;
        font-size: 1em;
        background-color: transparent;
        transition: 0.10s ease-in-out;
        color: white;
        width: 20%;
        display: inline-block;
        margin: 10px;
    }

    .input-style2::placeholder {
        color: rgb(206, 206, 206);
    }

    .input-style2:hover {
        border-bottom: 3px solid #a6a6a6;
        color: white;
    }

    .input-style2:focus {
        outline: 0;
        border: 0;
        border-bottom: 3px solid #0074D9;
        color: white;
    }

    .button-1 {
        width: 140px;
        height: 50px;
        border: 2px solid #fff;
        justify-content: center;
        text-align: center;
        cursor: pointer;
        position: relative;
        box-sizing: border-box;
        overflow: hidden;
        margin: 0px auto 20px;
    }

    .button-1 input {
        font-family: arial;
        font-size: 16px;
        color: #fff;
        text-decoration: none;
        line-height: 50px;
        transition: all .10s ease;
        z-index: 2;
        position: relative;
    }

    .eff-1 {
        width: 140px;
        height: 50px;
        top: -2px;
        right: -140px;
        background: #5674B9;
        position: absolute;
        transition: all .10s ease;
        z-index: 1;
    }

    .button-1:hover .eff-1 {
        right: 0;
    }

    .button-1:hover a {
        color: #fff;
        border: 0px;
    }

    .button-1 a {
        color: #fff;
        border: 0px;
    }

    /*  -------------------------------------------------------------   */

    @import "compass/css3";

    .cubeFace,
    .wrap .cube .face {
        position: absolute;
        width: 200px;
        height: 200px;
        line-height: 200px;
        text-align: center;
    }

    .wrap {
        perspective: 800px;
        perspective-origin: 50% 100px;
        margin-top: 40px;
    }

    .wrap .cube {
        position: relative;
        width: 200px;
        margin: 0 auto;
        transform-style: preserve-3d;
    }

    .wrap .cube .back {
        transform: translateZ(-100px) rotateY(180deg);
    }

    .wrap .cube .right {
        transform: rotateY(-270deg) translateX(100px);
        transform-origin: top right;
    }

    .wrap .cube .left {
        transform: rotateY(270deg) translateX(-100px);
        transform-origin: center left;
    }

    .wrap .cube .front {
        transform: translateZ(100px);
    }

    .wrap .cube .back2 {
        transform: translateZ(-100px) rotateY(180deg);
    }

    .wrap .cube .right2 {
        transform: rotateY(-270deg) translateX(100px);
        transform-origin: top right;
    }

    .wrap .cube .left2 {
        transform: rotateY(270deg) translateX(-100px);
        transform-origin: center left;
    }

    .wrap .cube .front2 {
        transform: translateZ(100px);
    }

    .wrap .cube .back3 {
        transform: translateZ(-100px) rotateY(180deg);
    }

    .wrap .cube .right3 {
        transform: rotateY(-270deg) translateX(100px);
        transform-origin: top right;
    }

    .wrap .cube .left3 {
        transform: rotateY(270deg) translateX(-100px);
        transform-origin: center left;
    }

    .wrap .cube .front3 {
        transform: translateZ(100px);
    }

    @keyframes spin {
        0% {
            transform: rotateY(0);
        }

        100% {
            transform: rotateY(360deg);
        }
    }

    .cube {
        animation: spin 13s infinite linear;
    }

    .cube .front {
        animation: 10s infinite linear;
        background: url("https://i.pinimg.com/originals/7e/1f/a4/7e1fa4b8b868ff67f02b8d2472c96dab.jpg");
        background-size: cover;
    }

    .cube .left {
        animation: 10s infinite linear;
        background: url("https://d3hne3c382ip58.cloudfront.net/resized/400x400/special-interests-and-hobbies-tours-in-provence-400X400_.JPG");
        background-size: cover;
    }

    .cube .back {
        animation: 10s infinite linear;
        background: url("https://d3hne3c382ip58.cloudfront.net/resized/400x400/wildlife-tours-in-denpasar-400X400_.JPG");
        background-size: cover;
    }

    .cube .right {
        animation: 10s infinite linear;
        background: url("https://southamericatourism.com/wp-content/uploads/2019/05/RIO_400x400_280109546.jpg");
        background-size: cover;
    }

    .hov:focus {
        border: 0px;

    }
</style>


<form class="Form" action="/result">
    <div class="contain">
        <br>
        <center>
            <h6 style="color: white;">Flying From</h6>
        </center>
        <input class="input-style country2" type="text" list="from_c" name="" placeholder="destination country"
            onchange="country2()" required>
        <div style="display:inline" class="cities2"></div>
        <center>
            <h6 style="color: white;">Flying To</h6>
        </center>
        <input class="input-style country1" list="from_c" type="text" name="" onchange="country1()"
            placeholder="destination country" required>
        <input id="finale123" type="text" name="flying_to"hidden>
        <input id="finale1" type="text" name="flying_from" hidden>
        <div style="display:inline" class="cities1"></div>
        <br>
        <br>
        <input class="input-style" type="date" name="flying_date" placeholder="Departing" required>
        <br>
        <br>
        <label
            style="border: 0;padding-bottom: .5em;font-size: 1em;background-color: transparent;transition: 0.10s ease-in-out;color: white;width: 20%;display: inline-block;margin: 10px;"
            class="input-style2" style="color:#FFF;">Adults</label>
        <label
            style="border: 0;padding-bottom: .5em;font-size: 1em;background-color: transparent;transition: 0.10s ease-in-out;color: white;width: 20%;display: inline-block;margin: 10px;"
            class="input-style2" style="color:#FFF;">Children</label>
        <label
            style="border: 0;padding-bottom: .5em;font-size: 1em;background-color: transparent;transition: 0.10s ease-in-out;color: white;width: 20%;display: inline-block;margin: 10px;"
            class="input-style2" style="color:#FFF;">Infants</label>
        <label
            style="border: 0;padding-bottom: .5em;font-size: 1em;background-color: transparent;transition: 0.10s ease-in-out;color: white;width: 20%;display: inline-block;margin: 10px;"
            class="input-style2" style="color:#FFF;">Travel Class</label>
        <br>
        <input min="1" name="adults" type="number" class="input-style2" value="1" required>
        <input min="0" name="children" value="0" type="number" class="input-style2" value="1" required>
        <input min="0" name="infants" value="0" type="number" class="input-style2" value="1" required>
        <select name="class" class="input-style2">
            <option style="color: black;" value="1">Business</option>
            <option style="color: black;" value="0">Best fare</option>
        </select>
        <br>
        <br>
        <div class="button-1">
            <div class="eff-1"></div>
            <input class="hov" style="background-color: transparent;border:0px;" value="Search Flight" type="submit">
        </div>
    </div>
</form>
<br>
<center>
    <h3 style="color:white;font-family:serif">Top Destinations</h3>
</center>
<div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4 col-sm-12">
        <div class="wrap">
            <div class="cube">
                <div class="front face"></div>
                <div class="back face"></div>
                <div class="left face"></div>
                <div class="right face"></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<datalist id="from_c">
    <?php foreach($countries as $country){
               echo'<option value="'.$country->country_code.'">'.$country->country_enName.'</option>';}?>
</datalist>
@extends ("partials.jslinks")
@section('extra_links')
<script>
    function country1(){
var xhr2 = new XMLHttpRequest();
xhr2.withCredentials = true;
co=$(".country1").val()
xhr2.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities1').html(this.responseText)
}

});

xhr2.open("GET", "http://localhost:8000/api/cities?co="+co+"&class=departure_city&class1=input-style");

xhr2.send();
}
function country2(){
var xhr3 = new XMLHttpRequest();
xhr3.withCredentials = true;
co1=$(".country2").val()
xhr3.addEventListener("readystatechange", function() {
if(this.readyState === 4) {

$('.cities2').html(this.responseText)
}
});

xhr3.open("GET", "http://localhost:8000/api/cities?co="+co1+"&class=arrival_city&class1=input-style d");

xhr3.send();
}
</script>
<script>
    function data(){
let $input = document.getElementById('my_input');
let $datalist = document.getElementById('browsers');
  let _value = null;

  let input_value = $input.value;
  let options = $datalist.children;
  let i = options.length;
  while(i--) {
    let option = options[i];

    if(option.value == input_value) {
      _value = option.getAttribute('data-value');

      break;
    }
  }

  if(_value == null) {
    console.warn('Value does not exist');
    return false;
  }
document.getElementById("finale123").value =_value ;
document.getElementById("finale123").innerHTML =_value ;
    }
    function data1(){
        let $input = document.getElementById('my_input1');
        let $datalist = document.getElementById('browsers1');
        let _value1 = null;

        let input_value = $input.value;
        let options = $datalist.children;
        let i = options.length;
        while(i--) {
        let option = options[i];

        if(option.value == input_value) {
        _value1 = option.getAttribute('data-value');

        break;
        }
        }

        if(_value1 == null) {
        console.warn('Value does not exist');
        return false;
        }
document.getElementById("finale1").value =_value1 ;
        document.getElementById("finale1").innerHTML =_value1 ;
        }

</script>
@endsection
@include ("partials.footer")
