<?php
use App\models\flights;
$flight=flights::find($_REQUEST['id']);
?>
@include ("partials.header")
<style>
    .StripeElement {
        box-sizing: border-box;
        height: 40px;
        width: 600px;
        padding: 10px 12px;
        border: 1px solid transparent;
        border-radius: 4px;
        background-color: white;
        box-shadow: 0 1px 3px 0 #e6ebf1;
        -webkit-transition: box-shadow 150ms ease;
        transition: box-shadow 150ms ease;
    }

    .StripeElement--focus {
        box-shadow: 0 1px 3px 0 #cfd7df;
    }

    .StripeElement--invalid {
        border-color: #fa755a;
    }

    .StripeElement--webkit-autofill {
        background-color: #fefde5 !important;
    }
</style>




<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Subscribe</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

<form id='payment-form'>
    <label>
        Card details
        <!-- placeholder for Elements -->
        <div id="card-element"></div>
    </label>
    <button  class="btn btn-success" type="submit">Submit Payment</button>
</form>


                </div>
            </div>
        </div>
    </div>
</div>

@extends ("partials.jslinks")
@section('extra_links')
<script src="https://js.stripe.com/v3/"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
var stripe =
Stripe('pk_test_51HZlEmKR4hiLKQ1vzX7CV10LQKBoBN4rlBFN5D5uZVC8Ouxd06dsYP0I53xVjfhuI6rqOe6AkxANoG7rHYfpnXcw0097vOGUHd');

var elements = stripe.elements();

// Set up Stripe.js and Elements to use in checkout form
var style = {
base: {
color: "#32325d",
fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
fontSmoothing: "antialiased",
fontSize: "16px",
"::placeholder": {
color: "#aab7c4"
}
},
invalid: {
color: "#fa755a",
iconColor: "#fa755a"
},
};

var cardElement = elements.create('card', {style: style});
cardElement.mount('#card-element');
var form = document.getElementById('payment-form');

form.addEventListener('submit', function(event) {
// We don't want to let default form submission happen here,
// which would refresh the page.
event.preventDefault();

stripe.createPaymentMethod({
type: 'card',
card: cardElement,
billing_details: {
// Include any additional collected billing details.
name: '{{auth()->user()->name}}',
},
}).then(stripePaymentMethodHandler);
});
function stripePaymentMethodHandler(result) {
if (result.error) {
console.log('error')
} else {
// Otherwise send paymentMethod.id to your server (see Step 4)
axios.post('/buy',{
payment_method_id: result.paymentMethod.id,
_token:'{{csrf_token()}}',
price:'{{$_REQUEST["price"]}}'
}).then(function (response) {
console.log(JSON.stringify(response.data));
saveticket(response.data)
})
.catch(function (error) {
console.log(error);
});
};

}
function saveticket(data){
    if(data.success==true){


var data = new FormData();
data.append("user_id", "{{auth()->user()->id}}");
data.append("_from", "{{$flight['_from']}}");
data.append("_to", "{{$flight['_to']}}");
data.append("departure_city", "{{$flight['departure_city']}}");
data.append("arrival_city", "{{$flight['arrival_city']}}");
data.append("date", "{{$flight['date']}}");
data.append("time", "{{$flight['departure_date']}}");
data.append("duration", "{{$flight['duration']}}");
data.append("passport_name", "{{$_REQUEST['name']}}");
data.append("passport_expiration_date", "{{$_REQUEST['Passport_expiration_date']}}");
data.append("passport_number", "{{$_REQUEST['passport_number']}}");
data.append("passport_image", "{{$imagenewname}}");
data.append("price", "{{$_REQUEST['price']}}");
data.append("phone", "{{$_REQUEST['phone']}}");
data.append("no_of_tickets", "{{$_REQUEST['no_of_tickets']}}");
data.append("status", "0");
var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function() {
if(this.readyState === 4) {
console.log(this.responseText);
}
});

xhr.open("POST", "http://localhost:8000/api/tickets");

xhr.send(data);
    }
    redirect()
}
function redirect(){
window.location.href = "/";
}
</script>

@endsection
