<?php
    if(isset($_REQUEST['color'])){
    $color=$_REQUEST['color'];
    $_SESSION['color']=$_REQUEST['color'];
    }elseif(isset($_SESSION['color'])){
    $color=$_SESSION["color"];
    }
    else{$color='white';}

    if($color=='Blue'){
         $footercolor='#206e6e';
         $link='https://pbs.twimg.com/media/DpMKIyAVAAIbsKv.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Yellow'){
         $footercolor='#f89d00';
         $link='https://i.pinimg.com/originals/02/5f/29/025f29a640db04a067d7a540a7b4d004.gif';
         $class='navbar-light';
         $textcolor='#ffffff';
     }

     if($color=='Pink'){
         $footercolor='#F6A2C6';
         $link='https://image.freepik.com/free-photo/travel-concept-airplane-pink-background-place-text_134398-5735.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Green'){
         $footercolor='#dff05f';
         $link='https://images.unsplash.com/photo-1530469641172-8ac15d0a7d6a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
    if($color=='white'){
         $footercolor='#520622';
         $link='https://cutewallpaper.org/21/plane-background-wallpaper/Best-64-Plane-Backgrounds-on-HipWallpaper-Art-Deco-.jpg';
         $class='navbar-light';
         $textcolor='#eeeeee';
     }
     if($color=='Dark'){
        $footercolor='#343A40';
         $link='https://www.chromethemer.com/download/hd-wallpapers/dark-paladin-1920x1080.jpg';
         $class='navbar-dark bg-dark';
         $textcolor='#17A2B8';
     }
    ?>
<style>
    .active2
{
    background-color:<?php echo $footercolor; ?>!important ;
}
    footer
    {
        background-color: <?php echo $footercolor;?>;
    }
    footer
    {
        position: relative;
        width: 100%;
        color: <?php echo $textcolor;?>;
    }
    .eff-1{
  width:140px;
  height:50px;
  top:-2px;
  right:-140px;
  position:absolute;
  transition:all .5s ease;
  z-index:1;
  background-color: <?php echo $footercolor;?>;
}
    .Form {
        max-width: 500px;
        margin: 40px auto 0;
        border: 1px solid <?php echo $textcolor;?>;
        }
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url("<?php echo $link; ?>");
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
    }
</style>

<footer>
  <div class="container py-3">
    <div class="row">
      <div class="col-12 col-md"> LOGO <small class="d-block my-3 text-muted">© 2020</small> </div>
      <div class="col-6 col-md">
        <h5><b>Features</b></h5>
        <ul class="list-unstyled text-small">
          <li>
            <a class="text-muted" href="#">Cool stuff</a>
          </li>
          <li>
            <a class="text-muted" href="#">Random feature</a>
          </li>
          <li>
            <a class="text-muted" href="#">Team feature</a>
          </li>
          <li>
            <a class="text-muted" href="#">Stuff for developers</a>
          </li>
          <li>
            <a class="text-muted" href="#">Another one</a>
          </li>
          <li>
            <a class="text-muted" href="#">Last time</a>
          </li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5><b>Resources</b></h5>
        <ul class="list-unstyled text-small">
          <li>
            <a class="text-muted" href="#">Resource</a>
          </li>
          <li>
            <a class="text-muted" href="#">Resource name</a>
          </li>
          <li>
            <a class="text-muted" href="#">Another resource</a>
          </li>
          <li>
            <a class="text-muted" href="#">Final resource</a>
          </li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5><b>Resources</b></h5>
        <ul class="list-unstyled text-small">
          <li>
            <a class="text-muted" href="#">Business</a>
          </li>
          <li>
            <a class="text-muted" href="#">Travel Portal</a>
          </li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5><b>About</b></h5>
        <ul class="list-unstyled text-small">
          <li>
            <a class="text-muted" href="#">Team</a>
          </li>
          <li>
            <a class="text-muted" href="#">Ahmed Yasser</a>
          </li>
          <li>
            <a class="text-muted" href="#">Yehia Yasser</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>
