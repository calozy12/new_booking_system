<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.14/css/intlTelInput.css" rel="stylesheet" />
        <link rel="shortcut icon" href="https://w7.pngwing.com/pngs/773/201/png-transparent-airplane-aircraft-flight-logo-airplane-blue-logo-flight.png">
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        @yield('extra_links1')
        <?php
        session_start();
            if(isset($_REQUEST['color'])){
            $color=$_REQUEST['color'];
            $_SESSION['color']=$_REQUEST['color'];
            }elseif(isset($_SESSION['color'])){
            $color=$_SESSION["color"];
            }
            else{$color='white';}

            if($color=='Blue'){
         $footercolor='#206e6e';
         $link='https://pbs.twimg.com/media/DpMKIyAVAAIbsKv.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Yellow'){
         $footercolor='#f89d00';
         $link='https://i.pinimg.com/originals/02/5f/29/025f29a640db04a067d7a540a7b4d004.gif';
         $class='navbar-light';
         $textcolor='#ffffff';
     }

     if($color=='Pink'){
         $footercolor='#F6A2C6';
         $link='https://image.freepik.com/free-photo/travel-concept-airplane-pink-background-place-text_134398-5735.jpg';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
     if($color=='Green'){
         $footercolor='#dff05f';
         $link='https://images.unsplash.com/photo-1530469641172-8ac15d0a7d6a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80';
         $class='navbar-light';
         $textcolor='#ffffff';
     }
    if($color=='white'){
         $footercolor='#520622';
         $link='https://cutewallpaper.org/21/plane-background-wallpaper/Best-64-Plane-Backgrounds-on-HipWallpaper-Art-Deco-.jpg';
         $class='navbar-light';
         $textcolor='#eeeeee';
     }
     if($color=='Dark'){
        $footercolor='#343A40';
         $link='https://www.chromethemer.com/download/hd-wallpapers/dark-paladin-1920x1080.jpg';
         $class='navbar-dark bg-dark';
         $textcolor='#17A2B8';
     }
    ?>
<style>
    .active2
{
    background-color:<?php echo $footercolor; ?>!important ;
}
    nav
    {
        background-color: <?php echo $footercolor;?>;
    }
    body {
        background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url("<?php echo $link; ?>");
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
    }
    .fontcolor{
    color:{{$textcolor}}
    }
</style>
</head>
<body>

    <nav class="navbar navbar-expand-md <?php echo $class;?> shadow-sm">
        <div class="container">
            <a style="color: <?php echo $textcolor;?>;" data-aos="fade-right" data-aos-delay="200" class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Book n Fly') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li style="color: <?php echo $textcolor;?>;" data-aos="fade-right" data-aos-delay="400" class="nav-item active">
                        <a style="color: <?php echo $textcolor;?>;" class="nav-link" href="/"><i style="color: <?php echo $textcolor;?>;" class="fas fa-1x fa-home"></i> | Home</a>
                      </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li style="z-index:1000;color: <?php echo $textcolor;?>;" data-aos="fade-left" data-aos-delay="600"  class="nav-item dropdown">
                            <a style="display:block;margin-top:10px;color:<?php echo $textcolor;?>;" class="dropdown-toggle dispp2" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Change Theme</a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                <form method="POST">
                                    @method('get')
                                    <input type="hidden"name="color"value="white">

                                    <button type="submit" class="dropdown-item">Default Theme</a>
                                     </form>
                                   <form method="POST">
                                       @method('get')
                                    <input type="hidden"name="color"value="Yellow">

                                    <button type="submit" class="dropdown-item">Theme 1</a>
                                    </form>
                             <form method="POST">
                                   @method('get')
                                    <input type="hidden"name="color"value="Blue">

                                    <button type="submit" class="dropdown-item">Theme 2</a>
                                    </form>
                             <form method="POST">
                                   @method('get')
                                    <input type="hidden"name="color"value="Green">

                                    <button type="submit" class="dropdown-item">Theme 3</a>
                                     </form>
                             <form method="POST">
                                   @method('get')
                                    <input type="hidden"name="color"value="Pink">

                                    <button type="submit" class="dropdown-item">Theme 4</a>
                                    </form>
                                    <form method="POST">
                                        @method('get')
                                         <input type="hidden"name="color"value="Dark">

                                         <button type="submit" class="dropdown-item">Dark Theme</a>
                                         </form>

                            </div>
                    </li>
                    &nbsp;
                    &nbsp;

                    <!-- Authentication Links -->
                    @guest
                        <li style="color: <?php echo $textcolor;?>;" data-aos="fade-left" data-aos-delay="1000" class="nav-item">
                            <a style="color: <?php echo $textcolor;?>;" class="nav-link" href="{{ route('login') }}"><i class="fas fa-1x fa-sign-in-alt"></i> | {{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li style="color: <?php echo $textcolor;?>;" data-aos="fade-left" data-aos-delay="1200" class="nav-item">
                                <a style="color: <?php echo $textcolor;?>;" class="nav-link" href="{{ route('register') }}"><i class="fas fa-1x fa-user-plus"></i> | {{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li style="color: <?php echo $textcolor;?>;" data-aos="fade-left" data-aos-delay="1000" class="nav-item dropdown">
                            <a style="color: <?php echo $textcolor;?>;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                @if(auth()->user()->role=='admin')
                                                    <a class="dropdown-item" href="/admin/dashboard">Dashboard</a>
                                                    @endif
                                                    @if(auth()->user()->role!='admin')
                                                    <a class="dropdown-item" href="/profile">profile</a>
                                                    @endif
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
            </div>
        </div>
    </nav>
