<?php
use App\models\cities;
?>
@include ("partials.header")
<div class="container"style="background-color:#c8d0d5;margin-top:2%;padding:10px;">
<center><h3 style="font-weight:lighter;">Bookings</h3></center>
@if(!isset($tickets[0]))
<center>
    <h1 class="fontcolor" style="font-weight:lighter">Sorry no Bookings here!</h1>
    <center>
        @endif
@foreach($tickets as $ticket)
<?php
$city=cities::find($ticket['departure_city']);
$city1=cities::find($ticket['arrival_city']);
?>

<div class="card text-white bg-info">
    <div class="card-body">
        <center><h5 class="card-title">Status:@if($ticket['status']==0) Pending @endif @if($ticket['status']==1) Accepted @endif @if($ticket['status']==2) Declined @endif</h5></center>
    <div class="row">
        <div class="col-md-4">
            <center><p class="card-text">from:{{$ticket->_from}},{{$city->name}}</p></center>
        </div>
        <div class="col-md-4">
            <center><p class="card-text">to:{{$ticket->_to}},{{$city1->name}}</p></center>
        </div>
        <div class="col-md-4">
            <center><p class="card-text">price:${{$ticket->price}}</p></center>
        </div>
        </div>
        <br>
    <div class="row">
        <div class="col-md-4">
            <center><p class="card-text">no of tickets:{{$ticket->no_of_tickets}}</p></center>
    </div>
    <div class="col-md-4">
        <center><p class="card-text">date:{{$ticket->date}},{{$ticket->time}}</p></center>
    </div>
    <div class="col-md-4">
        <center><p class="card-text">duration:{{$ticket->duration}}</p></center>
    </div>
</div>
<br>
</div>
    </div>
<br>
@endforeach
</div>
@include('partials.jslinks')
