<?php
use App\models\flights;
use App\models\cities;
$flight=flights::find($_GET['id']);
if($_GET['class']==1){
    $flightprice=$flight->buisness_price;
}
if($_GET['class']==0){
        $flightprice=$flight->best_fare;
}
$city=cities::find($flight->departure_city);
$city1=cities::find($flight->arrival_city);
$index_image=$flight->airline_logo;?>
@include ("partials.header")
<style>
        @media only screen and (min-width: 767px) {
        .rightsidebar {
            margin-top: -670px;
        }
    }
    @media only screen and (max-width: 768px) {
        .cll {
            display: none;
        }
    }

    @media only screen and (max-width: 768px) {
        .cll2 {
            display: inline-block;
        }
    }

    @media only screen and (min-width: 768px) {
        .cll2 {
            display: none;
        }
    }

    @media only screen and (max-width: 700px) {
        .fade {
            margin-bottom: 17px;
        }
    }

    @media only screen and (max-width: 576px) {
        .fade {
            width: 100%;
            display: inline-block;
        }

        .FADE2 {
            width: 25%;
            display: inline-block;
        }

        .FADE3 {
            width: 100%;
            display: block;
        }

        .cll2 {
            display: none;
        }
    }
    body
    {
        background-color: #E5F0F9;
    }
</style>
<br>
<div class="row">
    <div class="col-md-8">
        <div style="margin-bottom: 5px" class="container cll">
            <div class="row">
                <div style="background-color:white;border:2px solid #eee" class="col-md-3">
                    <center><h4 style="color: black;font-weight:lighter;">Airline</h4></center>
                </div>
                <div style="background-color:white;border:2px solid #eee" class="col-md-2">
                <center><h4 style="color: black;font-weight:lighter;">Depart</h4></center>
                </div>
                <div style="background-color:white;border:2px solid #eee" class="col-md-2">
                <center><h4 style="color: black;font-weight:lighter;">Duration</h4></center>
                </div>
                <div style="background-color:white;border:2px solid #eee" class="col-md-2">
                <center><h4 style="color: black;font-weight:lighter;">Arrive</h4></center>
                </div>
                <div style="background-color:white;border:2px solid #eee" class="col-md-3">
                <center><h4 style="color: black;font-weight:lighter;">Price</h4></center>
                </div>
            </div>
        </div>

                <div class="container">
                    <div class="row mb-1">
                    <div class="col-md-3  col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                           <center><img style='width:100px;' src="{{ asset ('storage/flights/'.$index_image) }}">
                            <br>
                            <br>
                            <h6>Flight Number:23432</h6>
                        </center>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                        <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">{{$city->name}}</h4><span style="display: inline-block">({{$flight->_from}})</span>
                            <h6 style="color: black;margin-bottom:10px;">{{$flight->date}}</h6>
                            <h6 style="color: black;">{{$flight->departure_date}}</h6>
                        </center>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                        <center><h5 style="color: black;margin-bottom:10px;">9:00 hour(s)</h5></center>
                            <center>
                                <div class="container">
                                    <hr>
                                </div>
                            </center>
                        <center><h5>0 stops</h5></center>
                        </div>
                        <div class="col-md-2 col-sm-3 col-xs-1 FADE2" style="background-color: white;border:2px solid #eee">
                            <center><h4 style="color: royalblue;margin-bottom:10px;display: inline-block">Madrid</h4><span style="display: inline-block">(Spain)</span>
                            <h6 style="color: black;margin-bottom:10px;">10/8/2020</h6>
                            <h6 style="color: black;">3:00 pm</h6>
                        </center>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 FADE3" style="background-color: white;border:2px solid #eee">
                            <center><h4 style="color: royalblue;">US 902$</h4><br></center>
                                <center><h6 style="color: black;">Refundable</h6><br></center>
                                    <center><a type="button" class="btn" style="background-color: #eee;margin:5px;"
                                href="/Traveller">Book Now</a></center>
                        </div>
                    </div>

                </div>
                </div>
    <div style="background-color:white;border:2px solid #e4e3e3;width:10%;height300px" class="col-md-3">
        <center>
            <h4>Fare Summary</h4>
        </center>
        <br>
        <h5 style="float:left">Base Fare</h5><br><br>
        <h6 style="float: left">1 Adult(s)(1 x price)</h6>
        <h6 style="float: right;margin:5px;">902$</h6>
        <br>
        <hr>
        <h5 style="float:left">Taxes</h5><br><br>
        <h6 style="float: left">Taxes & Fees</h6>
        <h6 style="float: right;margin:5px;">100$</h6>
        <br>
        <hr>
        <br>
        <h5 style="float: left">GRAND TOTAL</h5>
        <h5 style="float: right;margin:5px;">1002$</h5>


    </div>
    <div class="col-md-1">
    </div>
</div>
        @include ("partials.jslinks")
