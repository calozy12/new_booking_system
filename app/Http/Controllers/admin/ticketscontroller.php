<?php

namespace App\Http\Controllers\admin;
use App\models\tickets;
use App\models\flights;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ticketscontroller extends Controller
{
         public function dashboard(){
         return view('admin.dashboard');
    }
    public function index(){
         return view('admin.tickets.index');
    }
    public function store(Request $request){
        $ticket=new tickets;
$flight=flights::find($request->id);
            $index_image = $request->file('index_image');
            $imagenewname = rand(0, 1000000) . time() . $index_image->getClientOriginalName();
        $ticket->passport_image=$imagenewname;
        $ticket->_from=$flight->_from;
        $ticket->_to=$flight->_to;
        $ticket->departure_city=$flight->departure_city;
        $ticket->arrival_city=$flight->arrival_city;
        $ticket->date=$flight->date;
        $ticket->time=$flight->departure_date;
        $ticket->duration=$flight->duration;
        $ticket->passport_name=$request->passport_name;
        $ticket->passport_expiration_date=$request->passport_expiration_date;
        $ticket->price=$request->price;
        $ticket->no_of_tickets=$request->no_of_tickets;
        $ticket->status=0;
        $ticket->user_id=auth()->user();
        $ticket->save();
        return redirect()->to('index');
         return view('admin.tickets.create');
    }
    public function edit($id){
         return view('admin.tickets.edit',compact('id'));
    }
    public function show(){
         return view('admin.tickets.show');
    }
}
