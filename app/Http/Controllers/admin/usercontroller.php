<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class usercontroller extends Controller
{
    public function edit($id)
    {
        $users = User::find($id);
        return view('admin.users.edit', compact('users'));
    }
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();

       return redirect()->back();
}
public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->password = bcrypt($request->password);
        $user->save();

       return redirect('/admin/users');
}
public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();
       return redirect()->back();
}
public function index()
    {
        $users =User::all();

       return view('admin.users.index', compact('users'));
}
public function show($id)
{
    $user = User::find($id);
    return view('admin.users.show', compact('user'));
}
public function create()
{   $user = new User;
    return view('admin.users.create', compact('user'));
}
}
