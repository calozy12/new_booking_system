<?php

namespace App\Http\Controllers\admin;
use App\models\airlines;
use App\models\airports;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class flightscontroller extends Controller
{
         public function dashboard(){
         return view('admin.dashboard');
    }
    public function index(){
         return view('admin.flights.index');
    }
    public function create(){
  $airlines=airlines::all();
$airports=airports::get();
         return view('admin.flights.create',compact('airlines','airports'));
    }
    public function edit($id){
$airlines=airlines::all();
$airports=airports::get();
         return view('admin.flights.edit',compact('id','airlines','airports'));
    }
    public function show(){
         return view('admin.flights.show');
    }
}
