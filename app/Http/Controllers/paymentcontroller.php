<?php

namespace App\Http\Controllers;
use App\Models\User;
use Carbon\carbon;
use Illuminate\Http\Request;

class paymentcontroller extends Controller
{
    function __construct(){
$this->middleware('auth');
    }
    public function payment(Request $request){
$index_image = $request->file('passport_image');
$imagenewname = rand(0, 1000000) . time() . $index_image->getClientOriginalName();
$index_image->storeas('/public/flights', $imagenewname);
        $data=[
            'intent' => auth()->user()->createSetupIntent()
        ];
    $current = Carbon::now();
if($_POST['Passport_expiration_date']<$current->addMonths(6)){
    return redirect()->back()->withErrors(['your passport expiring date must be after more than 6 months', 'your passport expiring date must be after more than 6 months']);
}
else{
    return view('payment',compact('imagenewname'))->with($data);
}

    }
    public function buy(Request $request){
$json_obj=$request->all();
return view('buy',compact('json_obj'));
}
}
