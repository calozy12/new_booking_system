<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\tickets;
class ticketscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return tickets::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
return tickets::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
$ticket=tickets::create($request->all());
return 'success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket=tickets::find($id);
        return $ticket;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $ticket=tickets::find($id);
       return $ticket;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
$ticket=tickets::find($id);
if (null !== $request->file('passport_image')) {
            $index_image = $request->file('passport_image');
            $imagenewname = rand(0, 1000000) . time() . $index_image->getClientOriginalName();

            $index_image->storeas('/public/hotel_rooms', $imagenewname);
        }
        if (null == $request->passport_image) {
            $imagenewname = $request->passport_image1;
        }
        $ticket->update($request->all());
        $ticket->passport_image=$imagenewname;
        $ticket->save();
        return redirect()->to('/admin/tickets/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ticket=tickets::find($id);
        $ticket->delete();
        return 'success';
    }
}
