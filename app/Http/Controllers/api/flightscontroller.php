<?php

namespace App\Http\Controllers\api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\flights;
class flightscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return flights::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $flights = new flights;
        $date1 = $request->departure_date;
        $date2 = $request->arrival_date;
        $timestamp1 = strtotime($date1);
        $timestamp2 = strtotime($date2);
        $hours = abs($timestamp2 - $timestamp1);
        $hour = gmdate("H:i:s", $hours);
        $flights->date = $request->date;
        $flights->timezone = $request->timezone;
        $flights->_from = $request->_from;
        $flights->arrival_city = $request->arrival_city;
        $flights->departure_city = $request->departure_city;
        $flights->_to = $request->_to;
        $flights->departure_date = $request->departure_date;
        $flights->arrival_date = $request->arrival_date;
        $flights->buisness_price = $request->buisness_price;
        $flights->best_fare = $request->best_fare;
        $flights->duration = $hour;
        $flights->airline_id = $request->airline_id;
        $flights->airport_id = $request->airport_id;
        $flights->available_seats = $request->available_seats;
if (null !== $request->file('airline_logo')) {
            $index_image = $request->file('airline_logo');
            $imagenewname = rand(0, 1000000) . time() . $index_image->getClientOriginalName();

            $index_image->storeas('/public/flights', $imagenewname);
        }
        if (null != $request->airline_logo1) {
            $imagenewname = $request->airline_logo1;
        }
        $flights->airline_logo=$imagenewname;
        $flights->save();
return redirect()->to('/admin/flights/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $flight=flights::find($id);
        return $flight;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $flight=flights::find($id);
       return $flight;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
$flights=flights::find($id);
        $date1 = $request->departure_date;
        $date2 = $request->arrival_date;
        $timestamp1 = strtotime($date1);
        $timestamp2 = strtotime($date2);
        $hours = abs($timestamp2 - $timestamp1);
        $hour = gmdate("H:i:s", $hours);
        $flights->date = $request->date;
        $flights->timezone = $request->timezone;
        $flights->_from = $request->_from;
        $flights->arrival_city = $request->arrival_city;
        $flights->departure_city = $request->departure_city;
        $flights->_to = $request->_to;
        $flights->departure_date = $request->departure_date;
        $flights->arrival_date = $request->arrival_date;
        $flights->buisness_price = $request->buisness_price;
        $flights->best_fare = $request->best_fare;
        $flights->duration = $hour;
        $flights->airline_id = $request->airline_id;
        $flights->airport_id = $request->airport_id;
        $flights->available_seats = $request->available_seats;
if (null !== $request->file('airline_logo')) {
            $index_image = $request->file('airline_logo');
            $imagenewname = rand(0, 1000000) . time() . $index_image->getClientOriginalName();

            $index_image->storeas('/public/flights', $imagenewname);
        }
        if (null == $request->airline_logo) {
            $imagenewname = $request->airline_logo1;
        }
        $flights->airline_logo=$imagenewname;
        $flights->save();
        return redirect()->to('/admin/flights/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $flight=flights::find($id);
        $flight->delete();
        return 'success';
    }
}
