<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tickets extends Model
{
    use HasFactory;
        protected $fillable = [
        'user_id',
        '_from',
        '_to',
        'arrival_city',
        'departure_city',
        'date',
        'time',
        'duration',
        'passport_name',
        'passport_expiration_date',
        'passport_number',
        'passport_image',
        'price',
        'no_of_tickets',
        'phone',
        'status',
        ];
}
